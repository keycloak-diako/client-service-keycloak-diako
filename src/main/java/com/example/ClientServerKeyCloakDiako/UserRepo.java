package com.example.ClientServerKeyCloakDiako;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserEntity, String> {
    UserEntity getUserEntityById(String id);
}
