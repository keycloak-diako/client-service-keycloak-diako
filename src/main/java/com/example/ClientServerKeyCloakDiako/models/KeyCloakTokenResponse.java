package com.example.ClientServerKeyCloakDiako.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class KeyCloakTokenResponse {
    String accessToken;
    int expiresIn;
    int refreshExpiresIn;
    String refreshToken;
    String tokenType;
    int notBeforePolicy;
    String sessionState;
    String scope;

    @JsonCreator
    public KeyCloakTokenResponse(@JsonProperty("access_token") String accessToken,
                                 @JsonProperty("expires_in") int expiresIn,
                                 @JsonProperty("refresh_expires_in") int refreshExpiresIn,
                                 @JsonProperty("refresh_token") String refreshToken,
                                 @JsonProperty("token_type") String tokenType,
                                 @JsonProperty("not-before-policy") int notBeforePolicy,
                                 @JsonProperty("session_state") String sessionState,
                                 @JsonProperty("scope") String scope) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.refreshExpiresIn = refreshExpiresIn;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.notBeforePolicy = notBeforePolicy;
        this.sessionState = sessionState;
        this.scope = scope;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public int getRefreshExpiresIn() {
        return refreshExpiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public int getNotBeforePolicy() {
        return notBeforePolicy;
    }

    public String getSessionState() {
        return sessionState;
    }

    public String getScope() {
        return scope;
    }
}
