package com.example.ClientServerKeyCloakDiako.models;

import lombok.Data;

import java.util.List;
@Data
public class UserDTO {
    String firstName;
    String lastName;
    List<String> groupIds;

    public UserDTO(String firstName, String lastName, List<String> groupIds) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupIds = groupIds;
    }
}
