package com.example.ClientServerKeyCloakDiako.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.reactivestreams.Publisher;

import javax.persistence.ElementCollection;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class GroupDTO {
    @Id
    String id;
    String name;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    List<String> userIds;

    @JsonCreator
    public GroupDTO(@JsonProperty("id") String id, @JsonProperty("name") String name, @JsonProperty("userIds")List<String> userIds) {
        this.id = id;
        this.name = name;
        this.userIds = userIds;
    }

//    @JsonCreator
//    public GroupDTO(@JsonProperty("name") String name) {
//        this.id = UUID.randomUUID().toString();
//        this.name = name;
//        this.userIds = new ArrayList<>();
//    }
}
