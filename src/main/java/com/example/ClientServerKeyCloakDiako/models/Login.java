package com.example.ClientServerKeyCloakDiako.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Login {
    private String password;
    private String username;
}
