package com.example.ClientServerKeyCloakDiako.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.UUID;

@Data
@NoArgsConstructor
public class JwtResponse {

    private String title;
    private String jwtToken;

    @JsonCreator
    public JwtResponse(
            @JsonProperty("title")String title,
            @JsonProperty("fjwtToken")String jwtToken
    ) {
        this.title=title;
        this.jwtToken = jwtToken;
    }
}
