package com.example.ClientServerKeyCloakDiako.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateUser {
    String firstName;
    String lastName;
}
