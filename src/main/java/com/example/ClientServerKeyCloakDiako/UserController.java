package com.example.ClientServerKeyCloakDiako;

import com.example.ClientServerKeyCloakDiako.models.CreateUser;
import com.example.ClientServerKeyCloakDiako.models.GroupDTO;
import com.example.ClientServerKeyCloakDiako.models.KeyCloakTokenResponse;
import com.example.ClientServerKeyCloakDiako.models.Login;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    UserService userService;
    WebClientHandler webClientHandler;

    public UserController(UserService userService, WebClientHandler webClientHandler) {
        this.userService = userService;
        this.webClientHandler = webClientHandler;
    }

    @PostMapping("/adduser")
    public UserEntity createUserEntity(@RequestBody CreateUser createUser) {
        return userService.userNewEntity(createUser);
    }

    @GetMapping("/get_all")
    public List<UserEntity> getAll(){
        return userService.getAllUsers();
    }



    @GetMapping("/get_all_groups")
    public Flux<GroupDTO> getAllGroups() {
        return webClientHandler.getAllGroupDTOS();
    }

    @GetMapping("/find_group_by_name")
    public Mono<GroupDTO> getGroupByName(@RequestParam String name) {
        return webClientHandler.getGroupDTOByName(name);
    }

    @GetMapping("/add_group_to_user/{userId}")
    public UserEntity addGroupToUser(@PathVariable("userId") String userId, @RequestParam String name) {
        Mono<GroupDTO> group = webClientHandler.getGroupDTOByName(name);

        return userService.addGroupToUser(userId, group.toString());
    }

    @PostMapping("/add_user_to_group/{name}/{userId}")
    public Mono<GroupDTO> addUserIdToGroup(@PathVariable("name") String name,@PathVariable("userId") String userId){
        return webClientHandler.addUserToGroup(name, userId);
    }

    @PostMapping("/login_to_group_server_withe_KeyCloakToken_and_get_jwt")
    public Mono<KeyCloakTokenResponse> loginToGroupServer(@RequestBody Login login){
        return webClientHandler.loginAndGetKeyCloakToken("http://localhost:8000/", "Disney", "ankeborg", login);

    }


}
