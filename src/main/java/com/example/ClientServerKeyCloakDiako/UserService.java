package com.example.ClientServerKeyCloakDiako;

import com.example.ClientServerKeyCloakDiako.models.CreateUser;
import com.example.ClientServerKeyCloakDiako.models.GroupDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepo userRepo;

    @Autowired
    WebClientHandler webClientHandler;

    public UserService(UserRepo userRepo, WebClientHandler webClientHandler) {
        this.userRepo = userRepo;
        this.webClientHandler = webClientHandler;
    }


    public UserEntity userNewEntity(CreateUser createUser) {
        UserEntity user = new UserEntity(
                createUser.getFirstName(),
                createUser.getLastName()
        );

        userRepo.save(user);
        return user;
    }

    public List<UserEntity> getAllUsers() {
        return userRepo.findAll();

    }

    public UserEntity addGroupToUser(String id, String group) {
        UserEntity user = userRepo.getUserEntityById(id);
        user.getGroupIds().add(group);
        return userRepo.save(user);
    }





}
