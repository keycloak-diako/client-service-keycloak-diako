package com.example.ClientServerKeyCloakDiako;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ClientServerKeyCloakDiakoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientServerKeyCloakDiakoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demoData(UserRepo userRepo) {
		return args -> {
			userRepo.save(new UserEntity("Madeleine","Togård"));
			userRepo.save(new UserEntity("Edda","Dadoun-Hallin"));
			userRepo.save(new UserEntity("Diako","Ismail"));
		};
	}



}
