package com.example.ClientServerKeyCloakDiako;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Table(name = "users")
@Entity
@NoArgsConstructor
public class UserEntity {
    @Id
    String id;
    String firstName;
    String lastName;

    @ElementCollection
    List<String> groupIds;

    @JsonCreator
    public UserEntity(@JsonProperty("firstName") String firstName,
                      @JsonProperty("lastName") String lastName) {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupIds = new ArrayList<>();
    }
}
