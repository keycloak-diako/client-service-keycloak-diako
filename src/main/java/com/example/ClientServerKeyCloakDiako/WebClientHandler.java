package com.example.ClientServerKeyCloakDiako;

import com.example.ClientServerKeyCloakDiako.models.GroupDTO;
import com.example.ClientServerKeyCloakDiako.models.KeyCloakTokenResponse;
import com.example.ClientServerKeyCloakDiako.models.Login;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class WebClientHandler {


    //KeyCloak Token ------------------------------------------------------
    private static String globalToken;



    public Flux<GroupDTO> getAllGroupDTOS() {
        WebClient webClient = WebClient.create("http://localhost:8081/groups/all_groups");

        Flux<GroupDTO> list = webClient.get()
                .header("keycloak", "Bearer " + globalToken)
                .exchangeToFlux(response -> response.bodyToFlux(GroupDTO.class));

        return list;
    }

    public Mono<GroupDTO> getGroupDTOByName(String name) {
        WebClient webClient = WebClient.create("http://localhost:8081/groups/find_group_by_name/" + name);

        return webClient.get().header("keycloak", "Bearer " + globalToken)
                .exchangeToMono(response -> response.bodyToMono(GroupDTO.class)
                );

    }

    public Mono<GroupDTO> addUserToGroup(String name, String userId) {
        WebClient webClient = WebClient.create("http://localhost:8081/groups/add_user_to_group/" + name + "/" + userId);
        return webClient.post()
                .header("keycloak", "Bearer " + globalToken)
                .exchangeToMono(response -> response.bodyToMono(GroupDTO.class));
    }


    public static Mono<KeyCloakTokenResponse> loginAndGetKeyCloakToken(String keyCloakBaseUrl, String realm, String clientId, Login login) {
        WebClient webClient = WebClient.builder()
                .baseUrl(keyCloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();


        Mono<KeyCloakTokenResponse> token = webClient.post()
                .uri("auth/realms/" + realm + "/protocol/openid-connect/token")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("grant_type", "password")
                        .with("client_id", clientId)
                        .with("username", login.getUsername())
                        .with("password", login.getPassword())
                        .with("access_token", ""))
                .exchangeToFlux(clientResponse -> clientResponse.bodyToFlux(KeyCloakTokenResponse.class))
                .onErrorMap(e -> new Exception("Failed to aquire token", e))
                .last();


        //tar
        globalToken = token.map(t -> t.getAccessToken()).block();


        return token;
    }

}
